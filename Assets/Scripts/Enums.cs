﻿namespace UI
{
    public enum ActivityType
    {
        Main,
        Levels,
        Game,
        Win,
        Lose,
        Pause,
        Options,
        About,
        Logo
    }
}

namespace Game
{
    public enum Dirrection
    {
        NONE = -1,
        UP,
        RIGHT,
        DOWN,
        LEFT
    }

    public enum GameState
    {
        None,
        Start,
        Playing,
        Win,
        Lose
    }
}

namespace Data
{
    public enum CellType
    {
        Straight,
        Turn,
        Start,
        Finish
    }

    public enum Angle
    {
        A0      = 0,
        A90     = 90,
        A180    = 180,
        A270    = 270
    }

    public enum EnemyType
    {
        Simple

    }

    public enum CollectableType
    {
        Star

    }
}