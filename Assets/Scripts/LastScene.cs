﻿using Game;
using UnityEngine;
using Zenject;

public class LastScene : MonoBehaviour
{
    [Inject]
    protected IGame game;

    public void OnComplete()
    {
        game.State.Value = GameState.Win;
    }
    public class Factory : PlaceholderFactory<LastScene> { }
}