﻿using MoreMountains.NiceVibrations;

public interface IHapticsManager
{
    bool IsInitialized
    {
        get;
    }

    bool IsOn
    {
        get;
        set;
    }

    void TriggerHaptics(HapticTypes type);
    void TriggerVibrate();

    void OnDisable();
}