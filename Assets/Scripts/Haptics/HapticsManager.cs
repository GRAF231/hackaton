﻿using MoreMountains.NiceVibrations;

public class HapticsManager : IHapticsManager
{
    public bool IsInitialized
    {
        get;
    }

    public bool IsOn
    {
        get; 
        set;
    }

    public void OnDisable()
    {
        MMVibrationManager.iOSReleaseHaptics();
    }

    public void TriggerHaptics(HapticTypes type)
    {
        MMVibrationManager.Haptic(type);
    }

    public void TriggerVibrate()
    {
        MMVibrationManager.Vibrate();
    }
}
