﻿using Graf.Properties;
using UnityEngine;

public interface IAudioManager
{
    AudioClip this[string name]
    {
        get;
    }

    FloatHashed MusicVolume
    {
        get;
    }

    FloatHashed SoundVolume
    {
        get;
    }

    BoolHashed IsMute
    {
        get;
    }

    void PlayOneShot(string name);
    void PlayOneShot(string name, float pitch);
    void PlayOneShot(string name, float pitch, float volume);

    void PlayBackgroundMusic(string name);
    void StopBackgroundMusic();
}