﻿using Graf.Properties;
using UnityEngine;

public class AudioManager : IAudioManager
{
    protected GameObject music;
    public AudioClip this[string name]
    {
        get
        {
            return Resources.Load<AudioClip>($"Clips/{name}");
        }
    }

    public FloatHashed MusicVolume
    {
        get;
    } = new FloatHashed("MusicVolume");

    public FloatHashed SoundVolume
    {
        get;
    } = new FloatHashed("SoundVolume");

    public BoolHashed IsMute
    {
        get;
    } = new BoolHashed("IsMute");

    public void PlayOneShot(string name)
    {
        PlayOneShot(name, 1f, SoundVolume.Property);
    }

    public void PlayOneShot(string name, float pitch)
    {
        PlayOneShot(name, pitch, SoundVolume.Property);
    }

    public void PlayOneShot(string name, float pitch, float volume)
    {
        if (SoundVolume.Property > 0)
        {
            GameObject go = new GameObject("Audio: " + name);

            AudioSource source = go.AddComponent<AudioSource>();
            source.clip = this[name];
            source.volume = volume;
            source.pitch = pitch;
            source.Play();

            Object.Destroy(go, source.clip.length);
        }
    }

    public void PlayBackgroundMusic(string name)
    {
        if (MusicVolume.Property > 0)
        {
            music = new GameObject("Music: " + name);

            AudioSource source = music.AddComponent<AudioSource>();
            source.clip = this[name];
            source.volume = MusicVolume.Property;
            source.pitch = 1f;
            source.loop = true;
            source.Play();
        }
    }

    public void StopBackgroundMusic()
    {
        Object.Destroy(music);
    }
}
