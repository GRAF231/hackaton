﻿namespace Data
{
    public interface IProgress
    {
        int LastOpened
        {
            get;
        }
        int StarsSum
        {
            get;
        }
        LevelProgressInfo GetInfo(int level);
        void SetInfo(LevelProgressInfo info, int index);
    }
}