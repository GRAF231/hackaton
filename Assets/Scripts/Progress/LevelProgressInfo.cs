﻿namespace Data
{
    [System.Serializable]
    public class LevelProgressInfo
    {
        public bool isOpened;
        public int stars;
    }
}