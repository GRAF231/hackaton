﻿using AkilGames.Serializing;

using System.Collections.Generic;

using UnityEngine;

using Zenject;

namespace Data
{
    public class Progress : IProgress
    {
        protected List<LevelProgressInfo> levelInfos;
        
        public int StarsSum
        {
            get
            {
                var res = 0;
                for (int i = 0; i < levelInfos.Count; i++)
                    res += levelInfos[i].stars;
                return res;
            }
        }

        public int LastOpened
        {
            get
            {
                var res = 1;
                for (int i = 1; i < loader.Levels; i++)
                {
                    if (!levelInfos[i].isOpened)
                    {
                        res = i;
                    }
                }
                return res;
            }
        }

        readonly protected ILoader loader;
        
        [Inject]
        public Progress(ILoader loader)
        {
            this.loader = loader;

            LoadLevelProgress();

            if (levelInfos == null || levelInfos.Count == 0 || levelInfos.Count < loader.Levels)
            {
                DefaultProgress();
            }
        }

        void DefaultProgress()
        {
            var newInfo = new List<LevelProgressInfo>();
            for (int i = 0; i < loader.Levels; i++)
            {
                var levelInfo = new LevelProgressInfo();

                if (levelInfos != null && i < levelInfos.Count)
                {
                    levelInfo.isOpened = levelInfos[i].isOpened;
                    levelInfo.stars = levelInfos[i].stars;
                }
                else
                {
                    levelInfo.isOpened = false;
                    levelInfo.stars = 0;
                }

                newInfo.Add(levelInfo);
            }
            newInfo[0].isOpened = true;

            levelInfos = newInfo;
            SaveLevelProgress();
        }

        public LevelProgressInfo GetInfo(int level)
        {
            return levelInfos[level - 1];
        }

        public void SetInfo(LevelProgressInfo info, int index)
        {
            if (levelInfos[index - 1].stars < info.stars)
            {
                levelInfos[index - 1] = info;
            }
            levelInfos[index - 1].isOpened = info.isOpened || levelInfos[index - 1].isOpened;
            SaveLevelProgress();
        }


        const string LEVEL_PROGRESS_INFO_PATH = "LEVEL_PROGRESS_INFO";

        public void LoadLevelProgress()
        {
            levelInfos = Serializer.XMLDeserializeFromResources<List<LevelProgressInfo>>(LEVEL_PROGRESS_INFO_PATH);
        }

        public void SaveLevelProgress()
        {
            Serializer.XMLSerializeToResources(LEVEL_PROGRESS_INFO_PATH, levelInfos);
        }
    }
}