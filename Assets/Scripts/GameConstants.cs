﻿public static class GameConstants
{
    public const string LEVELS_FOLDER_PATH = "Levels/";
    public const string LEVELS_PATH = "Levels/Level";

    public const int MIN_WIDTH = 3;
    public const int MIN_HEIGHT = 3;

    public const int MAX_WIDTH = 10;
    public const int MAX_HEIGHT = 10;

    public const float CELL_WIDTH = 1.0f;
    public const float CELL_HEIGHT = 1.0f;


    public static string GetLevelPath(int levelIndex)
    {
        return $"{LEVELS_PATH}{levelIndex:d3}";
    }
}