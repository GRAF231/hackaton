﻿using UnityEngine;

using Zenject;

using DG.Tweening;

namespace Game
{
    public class Movable : MonoBehaviour, IPoolable<IMemoryPool>
    {
        [SerializeField]
        protected MovableView view;

        #region Move
        public virtual float Speed
        {
            get;
        }

        protected WayPoint prevPoint;
        protected WayPoint nextPoint;

        public virtual void Spawn(Cell cell)
        {
            transform.position = cell.transform.position;

            prevPoint = cell.First;
            MoveTo(cell.Last);
        }

        public virtual Tweener EndMoving(WayPoint point)
        {
            var length = Vector2.Distance(transform.position, point.transform.position);
            var time = length / Speed;

            transform.DOKill();
            return transform.DOMove(point.transform.position, time)
                            .SetEase(Ease.Linear);
        }

        public virtual void Stop()
        {
            transform.DOKill();
            view.Move(Dirrection.NONE);
        }

        protected virtual void MoveTo(WayPoint point)
        {
            if(prevPoint.Parrent != point.Parrent)
            {
                prevPoint.Parrent.IsNotEmpty = false;
                point.Parrent.IsNotEmpty = true;
            }

            nextPoint = point;
            view.Move(GetDirrection());

            var length = Vector2.Distance(transform.position, point.transform.position);
            var time = length / Speed;

            transform.DOKill();
            transform.DOMove(point.transform.position, time)
                     .SetEase(Ease.Linear)
                     .OnComplete(OnArival);
        }

        protected virtual void OnArival()
        {
            if (nextPoint.Next != null && nextPoint.Next != prevPoint)
            {
                prevPoint = nextPoint;
                MoveTo(nextPoint.Next);
            }
            else if(nextPoint.Prev != null && nextPoint.Prev != prevPoint)
            {
                prevPoint = nextPoint;
                MoveTo(nextPoint.Prev); 
            }
            else
            {
                var point = nextPoint;
                MoveTo(prevPoint);
                prevPoint = point;
            }
        }

        protected Dirrection GetDirrection()
        {
            if(Mathf.Abs(prevPoint.transform.position.x - nextPoint.transform.position.x) < 0.1f)
            {
                if (prevPoint.transform.position.y < nextPoint.transform.position.y)
                {
                    return Dirrection.UP;
                }
                else
                {
                    return Dirrection.DOWN;
                }
            }
            else
            {
                if (prevPoint.transform.position.x < nextPoint.transform.position.x)
                {
                    return Dirrection.RIGHT;
                }
                else
                {
                    return Dirrection.LEFT;
                }
            }
        }
        #endregion

        #region Pool
        IMemoryPool _pool;
        bool isSpawned;

        public virtual void Despawn()
        {
            if (!isSpawned)
            {
                return;
            }
            _pool.Despawn(this);
        }

        public virtual void OnDespawned()
        {
            isSpawned = false;
        }

        public virtual void OnSpawned(IMemoryPool _pool)
        {
            isSpawned = true;
            this._pool = _pool;
        }
        #endregion
    }
}