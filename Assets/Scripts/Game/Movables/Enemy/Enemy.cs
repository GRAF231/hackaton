﻿using UnityEngine;

using Zenject;

using Data;

namespace Game
{
    public class Enemy : Movable
    {
        public event System.Action OnPlayerHit;

        [Inject]
        protected EnemySettings settings;


        public override float Speed => settings.speed;

        protected EnemyType type;
        public void Init(EnemyInfo info)
        {
            type = info.type;
            transform.localPosition = new Vector3(info.spawnX, info.spawnY);
        }
        
        void OnTriggerEnter2D(Collider2D collision)
        {
            OnPlayerHit?.Invoke();
            Stop();
        }

        public class Factory : PlaceholderFactory<Enemy> { }
        public class Pool : MonoPoolableMemoryPool<IMemoryPool, Enemy> { }
    }
}