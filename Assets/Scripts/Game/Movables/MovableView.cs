﻿using UnityEngine;

namespace Game
{
    public class MovableView : MonoBehaviour
    {
        [SerializeField]
        protected Animator anim;

        [SerializeField]
        protected SpriteRenderer rend;

        public void Move(Dirrection dirrection)
        {
            anim.SetInteger("Dirrection", (int)dirrection);
        }
    }
}