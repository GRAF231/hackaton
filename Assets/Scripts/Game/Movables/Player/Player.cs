﻿using UnityEngine;

using Zenject;

using Data;

namespace Game
{
    public class Player : Movable
    {
        [Inject]
        protected PlayerSettings settings;

        public override float Speed => settings.speed;

        public class Factory : PlaceholderFactory<Player> { }
        public class Pool : MonoPoolableMemoryPool<IMemoryPool, Player> { }
    }
}