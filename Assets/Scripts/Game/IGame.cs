﻿using Graf.Properties;

namespace Game
{
    public interface IGame
    {
        bool IsPaused
        {
            get;
            set;
        }

        int Level
        {
            get;
        }
        EventProperty<int> Stars
        {
            get;
        }
        EventProperty<GameState> State
        {
            get;
        }


        void StartLevel(int index);

        void Restart();

        void Destroy();
    }
}