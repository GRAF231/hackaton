﻿using UnityEngine;
using Zenject;

namespace Game
{
    public class Star : Collectable, IPoolable<IMemoryPool>
    {
        public event System.Action OnCollect;

        #region Pool
        IMemoryPool pool;
        bool isSpawned;

        public virtual void Despawn()
        {
            if(!isSpawned)
            {
                return;
            }
            pool.Despawn(this);
            OnCollect = null;
        }

        public void OnDespawned()
        {
            isSpawned = false;
        }

        public void OnSpawned(IMemoryPool pool)
        {
            isSpawned = true;
            this.pool = pool;
        }
        #endregion

        void OnTriggerEnter2D(Collider2D collision)
        {
            OnCollect?.Invoke();
            Despawn();
        }

        public class Factory : PlaceholderFactory<Star> { }
        public class Pool : MonoPoolableMemoryPool<IMemoryPool, Star> { }
    }
}