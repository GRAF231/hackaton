﻿using Data;
using UnityEngine;
using Zenject;

namespace Game
{
    public class Collectable : MonoBehaviour
    {
        [Inject]
        protected IGame game;

        protected CollectableType type;

        public void Init(CollectableInfo info)
        {
            type = info.type;
            transform.localPosition = new Vector3(info.spawnX, info.spawnY);
        }
    }
}