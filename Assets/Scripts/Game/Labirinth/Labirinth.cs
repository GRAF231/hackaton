﻿using UnityEngine;

using Zenject;

using Data;

namespace Game
{
    public partial class Labirinth : MonoBehaviour
    {
        [Inject]
        protected Cell.Factory cellFactory;


        Cell[,] _cells;

        public Cell StartCell
        {
            get;
            protected set;
        }

        public Cell FinishCell
        {
            get;
            protected set;
        }

        public int Width => _cells.GetLength(0);

        public int Height => _cells.GetLength(1);

        public bool IsLocked
        {
            get;
            set;
        }

        public Cell this[int x, int y]
        {
            get
            {
                if(_cells == null)
                {
                    return null;
                }

                x = Mathf.Clamp(x, 0, Width - 1);
                y = Mathf.Clamp(y, 0, Height - 1);

                return _cells[x, y];
            }
        }


        public void BuildLevel(LevelInfo info)
        {
            IsLocked = false;

            BuildCells(info);
            BuildStart(info);
            BuildFinish(info);

            InitWayPoints();
        }

        public void DestroyLevel()
        {
            for (int x = 0; x < _cells.GetLength(0); x++)
            {
                for (int y = 0; y < _cells.GetLength(1); y++)
                {
                    RemoveCell(_cells[x, y]);
                }
            }
            RemoveCell(StartCell);
            RemoveCell(FinishCell);
        }

        public void RebuildConnections(Cell from)
        {
            if (from.X > 0)
            {
                TryToConnect(from, _cells[from.X - 1, from.Y]);
            }
            if (from.Y > 0)
            {
                TryToConnect(from, _cells[from.X, from.Y - 1]);
            }
            if (from.X < Width - 1)
            {
                TryToConnect(from, _cells[from.X + 1, from.Y]);
            }
            if (from.Y < Height - 1)
            {
                TryToConnect(from, _cells[from.X, from.Y + 1]);
            }
            if (from.X == 0 && from.Y == StartCell.Y)
            {
                TryToConnect(_cells[0, StartCell.Y], StartCell);
            }
            if (from.X == Width - 1 && from.Y == FinishCell.Y)
            {
                TryToConnect(_cells[Width - 1, FinishCell.Y], FinishCell);
            }
        }

        public void RemoveConnections(Cell from)
        {
            RemoveConnections(from.First);
            RemoveConnections(from.Last);
        }


        void BuildCells(LevelInfo info)
        {
            _cells = new Cell[info.width, info.height];
            for (int x = 0; x < info.width; x++)
            {
                for (int y = 0; y < info.height; y++)
                {
                    var cell = GetCell(info.cells[x + y * info.width].type);
                    cell.transform.SetParent(transform);
                    cell.Init(info.cells[x + y * info.width], this);

                    _cells[x, y] = cell;
                }
            }
        }

        void BuildStart(LevelInfo info)
        {
            var cell = GetCell(CellType.Start);
            cell.transform.SetParent(transform);
            cell.IsActive = false;
            cell.Init(new CellInfo
            {
                x = -1,
                y = info.startY
            }, this);

            StartCell = cell;
        }

        void BuildFinish(LevelInfo info)
        {
            var cell = GetCell(CellType.Finish);
            cell.transform.SetParent(transform);
            cell.IsActive = false;
            cell.Init(new CellInfo
            {
                x = Width,
                y = info.finishY
            }, this);

            FinishCell = cell;
        }

        Cell GetCell(CellType type)
        {
            return cellFactory.Create(type);
        }

        void RemoveCell(Cell cell)
        {
            if (cell != null)
            {
                Destroy(cell.gameObject);
            }
        }


        void InitWayPoints()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (x > 0)
                    {
                        TryToConnect(_cells[x, y], _cells[x - 1, y]);
                    }
                    if (y > 0)
                    {
                        TryToConnect(_cells[x, y], _cells[x, y - 1]);
                    }
                    if (x < Width - 1)
                    {
                        TryToConnect(_cells[x, y], _cells[x + 1, y]);
                    }
                    if (y < Height - 1)
                    {
                        TryToConnect(_cells[x, y], _cells[x, y + 1]);
                    }
                }
            }

            TryToConnect(_cells[0, StartCell.Y], StartCell);
            TryToConnect(_cells[Width - 1, FinishCell.Y], FinishCell);
        }

        void TryToConnect(Cell a, Cell b)
        {
            TryToConnect(a.First, b.First);
            TryToConnect(a.First, b.Last);
            TryToConnect(a.Last, b.First);
            TryToConnect(a.Last, b.Last);
        }

        void TryToConnect(WayPoint a, WayPoint b)
        {
            const float CONNECT_DISTANCE = GameConstants.CELL_WIDTH / 2.0f;

            if(Vector2.Distance(a.transform.position, b.transform.position) < CONNECT_DISTANCE)
            {
                if (a.Next == null)
                {
                    a.Next = b;
                }
                else if (a.Prev == null)
                {
                    a.Prev = b;
                }

                if (b.Next == null)
                {
                    b.Next = a;
                }
                else if (b.Prev == null)
                {
                    b.Prev = a;
                }
            }
        }

        void RemoveConnections(WayPoint from)
        {
            if (from.Next != null && from.Parrent != from.Next.Parrent)
            {
                if (from.Next.Prev == from)
                {
                    from.Next.Prev = null;
                }
                else if (from.Next.Next == from)
                {
                    from.Next.Next = null;
                }

                from.Next = null;
            }
            if (from.Prev != null && from.Parrent != from.Prev.Parrent)
            {
                if (from.Prev.Prev == from)
                {
                    from.Prev.Prev = null;
                }
                else if (from.Prev.Next == from)
                {
                    from.Prev.Next = null;
                }

                from.Prev = null;
            }
        }
    }
}