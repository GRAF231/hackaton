﻿using UnityEngine;

namespace Game
{
    public class WayPoint : MonoBehaviour
    {
        [SerializeField]
        protected Cell parrent;

        [SerializeField]
        protected WayPoint next;

        [SerializeField]
        protected WayPoint prev;


        public Cell Parrent => parrent;

        public WayPoint Next
        {
            get
            {
                return next;
            }
            set
            {
                if(value == this)
                {
                    return;
                }
                next = value;
            }
        }

        public WayPoint Prev
        {
            get
            {
                return prev;
            }
            set
            {
                if (value == this)
                {
                    return;
                }
                prev = value;
            }
        }
    }
}