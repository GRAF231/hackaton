﻿using UnityEngine;

namespace Game
{
    public partial class Labirinth : MonoBehaviour
    {
        [SerializeField]
        protected bool isGizmozDrawing = true;

        void OnDrawGizmos()
        {
            if (!isGizmozDrawing || _cells == null)
            {
                return;
            }
            Gizmos.color = Color.red;

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (_cells[x, y].First.Next != null)
                    {
                        Gizmos.DrawLine(_cells[x, y].First.transform.position, _cells[x, y].First.Next.transform.position);
                    }
                    if (_cells[x, y].First.Prev != null)
                    {
                        Gizmos.DrawLine(_cells[x, y].First.transform.position, _cells[x, y].First.Prev.transform.position);
                    }
                    if (_cells[x, y].Last.Next != null)
                    {
                        Gizmos.DrawLine(_cells[x, y].Last.transform.position, _cells[x, y].Last.Next.transform.position);
                    }
                    if (_cells[x, y].Last.Prev != null)
                    {
                        Gizmos.DrawLine(_cells[x, y].Last.transform.position, _cells[x, y].Last.Prev.transform.position);
                    }
                }
            }
        }
    }
}