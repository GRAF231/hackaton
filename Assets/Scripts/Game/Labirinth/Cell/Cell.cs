﻿using UnityEngine;
using UnityEngine.EventSystems;

using DG.Tweening;

using Zenject;

using Data;

namespace Game
{
    public partial class Cell : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField]
        protected float turnTime = 0.3f;

        [SerializeField]
        protected WayPoint[] wayPoints;

        
        protected Labirinth controller;


        public WayPoint First => wayPoints[0];

        public WayPoint Last => wayPoints[wayPoints.Length - 1];


        public bool IsTurning
        {
            get;
            protected set;
        }

        public bool IsActive
        {
            get;
            set;
        } = true;

        public bool IsNotEmpty
        {
            get;
            set;
        }

        public Angle Angle
        {
            get;
            protected set;
        }

        public int X
        {
            get;
            protected set;
        }

        public int Y
        {
            get;
            protected set;
        }


        public void Init(CellInfo info, Labirinth controller)
        {
            transform.DOKill();
            IsTurning = false;
            this.controller = controller;

            Angle = info.angle;
            X = info.x;
            Y = info.y;

            transform.localPosition = new Vector2((-controller.Width / 2 + 0.5f + X) * GameConstants.CELL_WIDTH, (-controller.Height / 2 + 0.5f + Y) * GameConstants.CELL_HEIGHT);
            transform.localEulerAngles = new Vector3(0.0f, 0.0f, -(int)Angle);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(IsTurning || !IsActive || IsNotEmpty || controller.IsLocked)
            {
                return;
            }
            DoTurn();
        }


        public void ReverseWayPoints()
        {
            var isForward = First.Next == wayPoints[1];

            if(isForward)
            {
                for (int i = 0; i < wayPoints.Length - 1; i++)
                {
                    wayPoints[i].Prev = wayPoints[i + 1];
                    wayPoints[i + 1].Next = wayPoints[i];
                }
            }
            else
            {
                for (int i = 0; i < wayPoints.Length - 1; i++)
                {
                    wayPoints[i].Next = wayPoints[i + 1];
                    wayPoints[i + 1].Prev = wayPoints[i];
                }
            }
        }


        protected virtual void DoTurn()
        {
            transform.DOKill();
            IsTurning = true;

            var angle = (int)transform.localEulerAngles.z;
            while (angle < 0)
            {
                angle += 360;
            }
            Angle = (Angle)angle;
            OnStartTurn();
            transform.DOLocalRotate(new Vector3(0.0f, 0.0f, 90.0f), turnTime, RotateMode.LocalAxisAdd)
                     .OnComplete(() =>
                     {
                         IsTurning = false;
                         OnEndTurn();
                     });
        }

        protected virtual void OnStartTurn()
        {
            controller.RemoveConnections(this);
        }
        
        protected virtual void OnEndTurn()
        {
            controller.RebuildConnections(this);
        }
        

        public class Factory : PlaceholderFactory<CellType, Cell>
        {
            protected readonly Prefabs prefabs;

            [Inject]
            public Factory(Prefabs prefabs)
            {
                this.prefabs = prefabs;
            }

            public override Cell Create(CellType type)
            {
                switch (type)
                {
                    case CellType.Straight:
                        return Instantiate(prefabs.cellStraightPrefab);

                    case CellType.Turn:
                        return Instantiate(prefabs.cellTurnPrefab);

                    case CellType.Start:
                        return Instantiate(prefabs.cellStartPrefab);

                    case CellType.Finish:
                        return Instantiate(prefabs.cellFinishPrefab);

                    default:
                        return null;
                }
            }
        }
    }
}