﻿using UnityEngine;

namespace Game
{
    public class FinishCell : Cell
    {
        public event System.Action OnEnter;

        void OnTriggerEnter2D(Collider2D collision)
        {
            OnEnter?.Invoke();
            Debug.Log("Win");
        }
    }
}