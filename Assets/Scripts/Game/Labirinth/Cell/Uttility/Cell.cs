﻿using UnityEngine;

using Zenject;

namespace Game
{
    public partial class Cell : MonoBehaviour
    {
        [SerializeField]
        protected bool isGizmozDrawing = true;

        void OnDrawGizmos()
        {
            if (!isGizmozDrawing)
            {
                return;
            }
            Gizmos.color = Color.cyan;

            for (int i = 0; i < wayPoints.Length - 1; i++)
            {
                if (wayPoints[i].Next != null)
                {
                    Gizmos.DrawLine(wayPoints[i].transform.position, wayPoints[i].Next.transform.position);
                }
                if (wayPoints[i].Prev != null)
                {
                    Gizmos.DrawLine(wayPoints[i + 1].transform.position, wayPoints[i + 1].Prev.transform.position);
                }
            }
        }
    }
}