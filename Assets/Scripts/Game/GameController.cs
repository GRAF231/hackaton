﻿using Data;
using Zenject;
using DG.Tweening;
using UnityEngine;

using Graf.Properties;
using System.Collections.Generic;

namespace Game
{
    public class GameController : IGame
    {
        protected readonly ILoader loader;
        protected readonly IProgress progress;
        protected readonly IAudioManager aManager;

        protected readonly Labirinth labirinth;

        protected readonly Player.Factory playerFactory;
        protected readonly Enemy.Factory enemyFactory;
        protected readonly Star.Factory starFactory;

        protected readonly LastScene.Factory last;

        [Inject]
        public GameController(ILoader loader, IProgress progress, Labirinth labirinth, Player.Factory playerFactory, Enemy.Factory enemyFactory, Star.Factory starFactory, IAudioManager aManager, LastScene.Factory last)
        {
            this.loader = loader;
            this.progress = progress;
            this.labirinth = labirinth;
            this.playerFactory = playerFactory;
            this.enemyFactory = enemyFactory;
            this.starFactory = starFactory;
            this.aManager = aManager;
            this.last = last;

            Stars = new EventProperty<int>();
            State = new EventProperty<GameState>();
        }
        
        bool isPaused;
        public bool IsPaused
        {
            get
            {
                return isPaused;
            }
            set
            {
                isPaused = true;
                Time.timeScale = isPaused ? 0.0f : 1.0f;
            }
        }

        public int Level
        {
            get;
            protected set;
        }

        public EventProperty<int> Stars
        {
            get;
        }

        public EventProperty<GameState> State
        {
            get;
        }
        
        Movable player;
        List<Star> stars = new List<Star>();
        List<Enemy> enemies = new List<Enemy>();

        public void Destroy()
        {
            (labirinth.FinishCell as FinishCell).OnEnter -= OnComplete;
            labirinth.DestroyLevel();
            if (player != null)
            {
                player.Despawn();
            }
            State.Value = GameState.None;
            for (int i = 0; i < stars.Count; i++)
            {
                stars[i].Despawn();
            }
            stars.Clear();
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].Despawn();
            }
            enemies.Clear();
        }

        public void Restart()
        {
            Destroy();
            StartLevel(Level);
        }

        void PlayBackMusic()
        {
            aManager.StopBackgroundMusic();
            aManager.PlayBackgroundMusic("SoundGame/BackSoundGame");
        }

        public void StartLevel(int index)
        {
            PlayBackMusic();

            Level = Mathf.Clamp(index, 1, loader.Levels);
            var info = loader.LoadLevel(Level);
            labirinth.BuildLevel(info);

            player = playerFactory.Create();
            player.Spawn(labirinth.StartCell);
            
            for (int i = 0; i < info.collectables.Length; i++)
            {
                if(info.collectables[i].type == CollectableType.Star)
                {
                    var star = starFactory.Create();
                    star.transform.position = new Vector2((-labirinth.Width / 2 + 0.5f + info.collectables[i].spawnX) * GameConstants.CELL_WIDTH, 
                                                          (-labirinth.Height / 2 + 0.5f + info.collectables[i].spawnY) * GameConstants.CELL_HEIGHT);

                    star.OnCollect += OnStarCollect;
                    stars.Add(star);
                }
            }

            for (int i = 0; i < info.enemies.Length; i++)
            {
                if (info.enemies[i].type == EnemyType.Simple)
                {
                    var enemy = enemyFactory.Create();
                    enemy.Spawn(labirinth[info.enemies[i].spawnX, info.enemies[i].spawnY]);

                    enemy.OnPlayerHit += OnPlayerHit;
                    enemies.Add(enemy);
                }
            }

            (labirinth.FinishCell as FinishCell).OnEnter += OnComplete;

            State.Value = GameState.Start;
            State.Value = GameState.Playing;
        }


        void OnStarCollect()
        {
            Stars.Value++;
            aManager.PlayOneShot("UI/PickUpStar");
        }

        void OnPlayerHit()
        {
            labirinth.IsLocked = true;
            player.Stop();
            State.Value = GameState.Lose;
            Stars.Value = 0;
        }

        void OnComplete()
        {
            labirinth.IsLocked = true;
            player.EndMoving(labirinth.FinishCell.Last)
                  .OnComplete(() =>
                  {
                      player.Stop();
                      if (Level == loader.Levels)
                      {
                          var scene = last.Create();
                      }
                      else
                      {
                          State.Value = GameState.Win;
                          Stars.Value = 0;
                      }
                  });

            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].Stop();
            }

            var info = new LevelProgressInfo
            {
                isOpened = true,
                stars = Stars.Value
            };
            progress.SetInfo(info, Level);

            if (Level + 1 <= loader.Levels)
            {
                var nextInfo = progress.GetInfo(Level + 1);
                nextInfo.isOpened = true;

                progress.SetInfo(nextInfo, Level + 1);
            }
        }
    }
}