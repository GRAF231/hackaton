﻿namespace Data
{
    [System.Serializable]
    public class LevelInfo
    {
        public int width;
        public int height;

        public CellInfo[] cells = new CellInfo[0];

        public int startY;
        public int finishY;

        public EnemyInfo[] enemies = new EnemyInfo[0];

        public CollectableInfo[] collectables = new CollectableInfo[0];
        

        public void SetSize(int width, int height)
        {
            this.width = width;
            this.height = height;

            cells = new CellInfo[width * height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    cells[x + y * width] = new CellInfo
                    {
                        x = x,
                        y = y
                    };
                }
            }
        }

        public CellInfo GetCell(int x, int y)
        {
            return cells[x + y * width];
        }

        public void SetCell(int x, int y, CellType type, Angle angle)
        {
            int index = x + y * width;

            cells[index].type = type;
            cells[index].angle = angle;
        }
    }
}