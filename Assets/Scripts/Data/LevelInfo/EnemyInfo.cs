﻿namespace Data
{
    [System.Serializable]
    public class EnemyInfo
    {
        public EnemyType type;

        public int spawnX;
        public int spawnY;
    }
}