﻿namespace Data
{
    [System.Serializable]
    public class CollectableInfo
    {
        public CollectableType type;

        public int spawnX;
        public int spawnY;
    }
}