﻿using UnityEngine;
using UnityEditor;

using AkilGames.Serializing;

namespace Data.Editor
{
    public class LevelEditorWindow : EditorWindow
    {
        const float CellSize = 40.0f;
        const float BorderSize = 5.0f;

        [MenuItem("Game/Open level editor #&l")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(LevelEditorWindow), false, "Level editor");
        }

        LevelInfo levelInfo;
        CellType cellType;
        Angle angle;
        CollectableType collectableType;
        EnemyType enemyType;
        void OnGUI()
        {
            if (levelInfo == null)
            {
                levelInfo = new LevelInfo();
                levelInfo.SetSize(GameConstants.MIN_WIDTH, GameConstants.MIN_HEIGHT);
            }

            DrawHeader();
            using (var horizontalScope = new GUILayout.HorizontalScope(GUILayout.Height(CellSize * levelInfo.height + 15)))
            {
                DrawField();
                DrawCellSettings();
            }
            DrawFooter();
            DrawCollectables();
            DrawEnemies();
        }

        void DrawHeader()
        {
            using (var horizontalScope = new GUILayout.HorizontalScope())
            {
                using (var verticalScope = new GUILayout.VerticalScope())
                {
                    GUILayout.Label("Width", EditorStyles.boldLabel);
                    var width = EditorGUILayout.IntField(Mathf.Clamp(levelInfo.width, GameConstants.MIN_WIDTH, GameConstants.MAX_WIDTH));
                    if (width != levelInfo.width)
                    {
                        levelInfo.SetSize(width, levelInfo.height);
                    }
                }
                using (var verticalScope = new GUILayout.VerticalScope())
                {
                    GUILayout.Label("Height", EditorStyles.boldLabel);
                    var height = EditorGUILayout.IntField(Mathf.Clamp(levelInfo.height, GameConstants.MIN_HEIGHT, GameConstants.MAX_HEIGHT));
                    if (height != levelInfo.height)
                    {
                        levelInfo.SetSize(levelInfo.width, height);
                    }
                }
            }
        }

        void DrawField()
        {
            GUILayout.Space(20);
            using (var verticalScopeField = new GUILayout.VerticalScope(GUILayout.Width(CellSize * levelInfo.width),
                                                                        GUILayout.Height(CellSize * levelInfo.height)))
            {
                for (int y = 0; y < levelInfo.height; y++)
                {
                    using (var horizontalScopeField = new GUILayout.HorizontalScope())
                    {
                        for (int x = 0; x < levelInfo.width; x++)
                        {
                            var rect = new Rect(x * CellSize, CellSize * levelInfo.height - y * CellSize + 20, CellSize, CellSize);

                            Texture2D tex = null;
                            var cell = levelInfo.GetCell(x, y);
                            switch (cell.type)
                            {
                                case CellType.Straight:
                                    tex = Resources.Load<Sprite>("Shapes/Straight").texture;
                                    break;

                                case CellType.Turn:
                                    tex = Resources.Load<Sprite>("Shapes/Turn").texture;
                                    break;
                            }

                            DrawBrick(rect, x, y);

                            var iniMatrix = GUI.matrix;
                            var color = GUI.color;

                            if (x == 0 && y == levelInfo.startY)
                            {
                                GUI.color = Color.red;
                            }
                            if (x == levelInfo.width - 1 && y == levelInfo.finishY)
                            {
                                GUI.color = Color.green;
                            }
                            var angle = (int)cell.angle;
                            GUIUtility.RotateAroundPivot(angle, new Vector2(rect.x + rect.width / 2f, rect.y + rect.height / 2f));

                            var offstep = 2.0f;
                            GUI.DrawTexture(new Rect(rect.x + offstep, rect.y + offstep, rect.width - offstep * 2, rect.height - offstep * 2), tex);
                            GUI.color = color;
                            GUI.matrix = iniMatrix;
                        }
                    }
                }
            }
        }

        void DrawBrick(Rect rect, int x, int y)
        {
            if (GUI.Button(rect, ""))
            {
                var isCollectable = Event.current.shift;
                var isEnemy = Event.current.alt;

                if (isCollectable)
                {
                    if(!IsCellEmpty(x, y))
                    {
                        return;
                    }
                    System.Array.Resize(ref levelInfo.collectables, levelInfo.collectables.Length + 1);
                    levelInfo.collectables[levelInfo.collectables.Length - 1] = new CollectableInfo
                    {
                        type = collectableType,
                        spawnX = x,
                        spawnY = y
                    };
                }
                else if(isEnemy)
                {
                    if (!IsCellEmpty(x, y))
                    {
                        return;
                    }
                    System.Array.Resize(ref levelInfo.enemies, levelInfo.enemies.Length + 1);
                    levelInfo.enemies[levelInfo.enemies.Length - 1] = new EnemyInfo
                    {
                        type = enemyType,
                        spawnX = x,
                        spawnY = y
                    };
                }
                else
                {
                    levelInfo.SetCell(x, y, cellType, angle);
                }
            }
        }

        void DrawCellSettings()
        {
            using (var verticalScopeSettings = new GUILayout.VerticalScope(GUILayout.Width(150.0f)))
            {
                GUILayout.Space(15);

                // Start/Finish
                GUILayout.BeginVertical("Exit", GUI.skin.box);
                GUILayout.Space(15);

                GUILayout.Label("Start Y:", EditorStyles.boldLabel);
                levelInfo.startY = EditorGUILayout.IntField(Mathf.Clamp(levelInfo.startY, 0, levelInfo.height - 1));

                GUILayout.Label("Finish Y:", EditorStyles.boldLabel);
                levelInfo.finishY = EditorGUILayout.IntField(Mathf.Clamp(levelInfo.finishY, 0, levelInfo.height - 1));

                GUILayout.EndVertical();

                GUILayout.Space(10);

                // Bricks
                GUILayout.BeginVertical("", GUI.skin.box);
                GUILayout.Label("Brick type", EditorStyles.boldLabel);
                var types = new string[] { "Straight", "Turn" };
                cellType = (CellType)EditorGUILayout.Popup((int)cellType, types);

                GUILayout.Label("Brick rotation", EditorStyles.boldLabel);
                angle = (Angle)EditorGUILayout.EnumPopup(angle);

                GUILayout.EndVertical();

                GUILayout.Space(10);

                // Collectables
                GUILayout.BeginVertical("", GUI.skin.box);
                GUILayout.Label("Collectable type (Shift)", EditorStyles.boldLabel);
                collectableType = (CollectableType)EditorGUILayout.EnumPopup(collectableType);

                GUILayout.EndVertical();

                GUILayout.Space(10);

                // Enemies
                GUILayout.BeginVertical("", GUI.skin.box);
                GUILayout.Label("Enemy type (Alt)", EditorStyles.boldLabel);
                enemyType = (EnemyType)EditorGUILayout.EnumPopup(enemyType);

                GUILayout.EndVertical();
            }
        }

        void DrawFooter()
        {
            GUILayout.Space(20);

            using (var horizontalScope = new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Clear"))
                {
                    levelInfo = new LevelInfo();
                }
                if (GUILayout.Button("Save"))
                {
                    var path = Application.dataPath + @"\Resources\Levels";
                    path = EditorUtility.SaveFilePanel("Save level", path, "Level001", "txt");

                    if (string.IsNullOrEmpty(path))
                    {
                        return;
                    }

                    Serializer.XMLSerialize(path, levelInfo);
                    AssetDatabase.Refresh();
                }
                if (GUILayout.Button("Load"))
                {
                    var path = Application.dataPath + @"\Resources\Levels";
                    path = EditorUtility.OpenFilePanelWithFilters("Save level", path, new string[] { "Text files", "txt", "All files", "*" });

                    if (string.IsNullOrEmpty(path))
                    {
                        return;
                    }

                    var loaded = Serializer.XMLDeserialize<LevelInfo>(path);
                    if (loaded == null)
                    {
                        return;
                    }
                    levelInfo = loaded;
                }
            }
        }

        void DrawCollectables()
        {
            for (int i = 0; i < levelInfo.collectables.Length; i++)
            {
                switch (levelInfo.collectables[i].type)
                {
                    case CollectableType.Star:
                        var star = levelInfo.collectables[i];

                        var tex = Resources.Load<Sprite>("Shapes/Star").texture;
                        var rect = new Rect(star.spawnX * CellSize + CellSize / 4, CellSize * levelInfo.height - star.spawnY * CellSize + 20 + CellSize / 4, CellSize / 2, CellSize / 2);

                        GUI.DrawTexture(rect, tex);
                        break;
                }
            }
        }
        
        void DrawEnemies()
        {
            for (int i = 0; i < levelInfo.enemies.Length; i++)
            {
                switch (levelInfo.enemies[i].type)
                {
                    case EnemyType.Simple:
                        var enemySimple = levelInfo.enemies[i];

                        var tex = Resources.Load<Sprite>("Shapes/SimpleEnemy").texture;
                        var rect = new Rect(enemySimple.spawnX * CellSize + CellSize / 4, CellSize * levelInfo.height - enemySimple.spawnY * CellSize + 20 + CellSize / 4, CellSize / 2, CellSize / 2);

                        GUI.DrawTexture(rect, tex);
                        break;
                }
            }
        }

        bool IsCellEmpty(int x, int y)
        {
            for (int i = 0; i < levelInfo.enemies.Length; i++)
            {
                if (levelInfo.enemies[i].spawnX == x && levelInfo.enemies[i].spawnY == y)
                {
                    return false;
                }
            }
            for (int i = 0; i < levelInfo.collectables.Length; i++)
            {
                if (levelInfo.collectables[i].spawnX == x && levelInfo.collectables[i].spawnY == y)
                {
                    return false;
                }
            }
            return true;
        }
    }
}