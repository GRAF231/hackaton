﻿namespace Data
{
    [System.Serializable]
    public class CellInfo
    {
        public CellType type;
        public Angle angle;
        public int x;
        public int y;
    }
}