using System.Collections.Generic;
using UI;
using UnityEngine;

using Zenject;

namespace Game
{
    [CreateAssetMenu(fileName = "GameData", menuName = "Installers/GameData")]
    public class GameDataInstaller : ScriptableObjectInstaller<GameDataInstaller>
    {
        [SerializeField]
        protected Prefabs prefabs;

        [SerializeField]
        protected PlayerSettings playerSettings;

        [SerializeField]
        protected EnemySettings enemySettings;


        public override void InstallBindings()
        {
            Container.BindInstance(prefabs);
            Container.BindInstance(playerSettings);
            Container.BindInstance(enemySettings);
        }
    }

    [System.Serializable]
    public class Prefabs
    {
        public Labirinth labirinthPrefab;
        public GameObject lastScenePrefab;


        [Header("Cells prefabs")]
        public Cell cellStraightPrefab;
        public Cell cellTurnPrefab;
        public Cell cellStartPrefab;
        public Cell cellFinishPrefab;


        [Header("Movable prefabs")]
        public Player playerPrefab;
        public Enemy enemyPrefab;


        [Header("Collectable prefabs")]
        public Star starPrefab;

        [Header("LevelButton prefabs")]
        public Level levelButtonPrefab;
    }
    
    [System.Serializable]
    public class PlayerSettings
    {
        public float speed = 2.0f;
    }

    [System.Serializable]
    public class EnemySettings
    {
        public float speed = 1.0f;
    }
}