﻿namespace Data
{
    public interface ILoader
    {
        int Levels
        {
            get;
        }

        LevelInfo LoadLevel(int index);
    }
}