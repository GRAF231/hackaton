﻿using UnityEngine;

using AkilGames.Serializing;

namespace Data
{
    public class LevelInfoLoader : ILoader
    {
        const string LEVEL_INFO_PATH = "LEVEL_INFO";

        public int Levels
        {
            get;
        }

        public LevelInfoLoader()
        {
            Levels = Resources.LoadAll(GameConstants.LEVELS_FOLDER_PATH).Length;
            Resources.UnloadUnusedAssets();
        }

        public LevelInfo LoadLevel(int index)
        {
            index = Mathf.Clamp(index, 1, index);
            var path = GameConstants.GetLevelPath(index);
            var info = Serializer.XMLDeserializeFromResources<LevelInfo>(path);

            return info;
        }
    }
}