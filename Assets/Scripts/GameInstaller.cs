using UnityEngine;
using Zenject;

using Data;
using UI;
using Game;


public class GameInstaller : MonoInstaller
{
    [Inject]
    protected Prefabs prefabs;

    public override void InstallBindings()
    {
        InstallServices();
        InstallGame();
        InstallCells();
        InstallMovables();
        InstallCollectables();
        InstallButtons();


        Container.BindFactory<LastScene, LastScene.Factory>()
                 .FromComponentInNewPrefab(prefabs.lastScenePrefab)
                 .UnderTransformGroup("---===Game===---");
    }

    void InstallServices()
    {
        Container.Bind<ILoader>()
                 .To<LevelInfoLoader>()
                 .AsSingle();

        Container.Bind<IProgress>()
                 .To<Progress>()
                 .AsSingle();

        Container.Bind<IGame>()
                 .To<GameController>()
                 .AsSingle()
                 .NonLazy();

        Container.Bind<IAudioManager>()
                 .To<AudioManager>()
                 .AsSingle()
                 .NonLazy();

        Container.Bind<MenuManager>()
                 .AsSingle()
                 .NonLazy();
    }

    void InstallGame()
    {
        Container.Bind<Labirinth>()
                 .FromComponentInNewPrefab(prefabs.labirinthPrefab)
                 .WithGameObjectName("Labirinth")
                 .UnderTransformGroup("---===Game===---")
                 .AsSingle();
    }

    void InstallCells()
    {
        Container.BindFactory<CellType, Cell, Cell.Factory>()
                 .FromComponentInNewPrefab(prefabs.cellStartPrefab)
                 .UnderTransformGroup("---===Game===---");
    }

    void InstallMovables()
    {
        Container.BindFactory<Player, Player.Factory>()
                 .FromPoolableMemoryPool<Player, Player.Pool>(poolBinder => poolBinder
                 .WithInitialSize(1)
                 .FromComponentInNewPrefab(prefabs.playerPrefab)
                 .UnderTransformGroup("---===Game===---"));

        Container.BindFactory<Enemy, Enemy.Factory>()
                 .FromPoolableMemoryPool<Enemy, Enemy.Pool>(poolBinder => poolBinder
                 .WithInitialSize(1)
                 .FromComponentInNewPrefab(prefabs.enemyPrefab)
                 .UnderTransformGroup("---===Game===---"));

    }

    void InstallCollectables()
    {
        Container.BindFactory<Star, Star.Factory>()
                 .FromPoolableMemoryPool<Star, Star.Pool>(poolBinder => poolBinder
                 .WithInitialSize(1)
                 .FromComponentInNewPrefab(prefabs.starPrefab)
                 .UnderTransformGroup("---===Game===---"));
    }

    void InstallButtons()
    {
        Container.BindFactory<Level, Level.Factory>()
                 .FromComponentInNewPrefab(prefabs.levelButtonPrefab);
    }
}