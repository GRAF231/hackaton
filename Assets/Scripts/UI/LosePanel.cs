﻿using Game;
using Zenject;
namespace UI
{
    public class LosePanel : UIPanel
    {
        [Inject]
        protected IGame gManager;

        [Inject]
        protected IAudioManager aManager;

        private void Awake()
        {
            menuManager.AddPanel(type, this);
            HideUI();

            gManager.State.OnPropertyChange += state =>
            {
                if (state == GameState.Lose)
                {
                    ShowUI();
                }
            };
        }

        public void ToMainMenu()
        {
            aManager.PlayOneShot("UI/Klick");
            gManager.Destroy();
            menuManager.SwitchPanel(ActivityType.Main);
        }

        public void Restart()
        {
            aManager.PlayOneShot("UI/Klick");
            gManager.Restart();
            menuManager.SwitchPanel(ActivityType.Game);
        }

        public override void ShowUI()
        {
            base.ShowUI();
            aManager.PlayOneShot("UI/GameOver");
        }
    }
}