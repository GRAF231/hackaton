﻿using Game;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace UI
{
    public class MenuManager
    {
        Dictionary<ActivityType, IPanel> panels = new Dictionary<ActivityType, IPanel>();

        public void AddPanel(ActivityType type, IPanel panel)
        {
            panels.Add(type, panel);
        }


        void HideAllPanels()
        {
            foreach(var panel in panels)
            {
                panel.Value.HideUI();
            }
        }
        
        public void SwitchPanel(ActivityType type)
        {
            HideAllPanels();
            panels[type].ShowUI();
        }
    }
}