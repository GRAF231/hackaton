﻿using Game;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace UI
{
    public class GamePanel : UIPanel
    {
        [SerializeField]
        protected List<GameObject> stars;

        [Inject]
        protected IGame game;

        [Inject]
        protected IAudioManager aManager;

        private void Awake()
        {
            menuManager.AddPanel(type, this);
            HideUI();

            game.Stars.OnPropertyChange += StarsOn;
        }

        void StarsOff(int count)
        {
            for (int i = 0; i < count; i++)
                stars[i].SetActive(false);
        }
        void StarsOn(int count)
        {
            StarsOff(stars.Count);

            for (int i = 0; i < count; i++)
                stars[i].SetActive(true);
        }
        


        public void ToPause()
        {
            aManager.PlayOneShot("UI/Klick");
            menuManager.SwitchPanel(ActivityType.Pause);
        }
    }
}