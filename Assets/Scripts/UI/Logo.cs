﻿using Zenject;
using UnityEngine;
using System.Collections;

namespace UI
{
    public class Logo : UIPanel
    {

        private void Awake()
        {
            menuManager.AddPanel(type, this);
            StartCoroutine(StartGame());
        }

        IEnumerator StartGame()
        {
            yield return new WaitForSeconds(2);

            menuManager.SwitchPanel(ActivityType.Main);
        }
    }
}