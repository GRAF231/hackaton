﻿using Game;
using Zenject;

namespace UI
{
    public class OptionsPanel : UIPanel
    {
        [Inject]
        protected IAudioManager aManager;


        public void SetSound()
        {
            aManager.PlayOneShot("UI/Klick");
            if (aManager.SoundVolume.Property > 0)
                aManager.SoundVolume.Property = 0;
            else
                aManager.SoundVolume.Property = 1f;
        }
        public void SetMusic()
        {
            aManager.PlayOneShot("UI/Klick");
            if (aManager.MusicVolume.Property > 0)
            {
                aManager.StopBackgroundMusic();
                aManager.MusicVolume.Property = 0;
            }
            else
                aManager.MusicVolume.Property = 1f;
        }

        public void SetVibro()
        {
            aManager.PlayOneShot("UI/Klick");
            aManager.IsMute.Property = !aManager.IsMute.Property;
        }

        public void Back()
        {
            aManager.PlayOneShot("UI/close");
            menuManager.SwitchPanel(ActivityType.Main);
        }
    }
}
