﻿namespace UI
{
    public interface IPanel
    {
        void ShowUI();
        void HideUI();
    }
}