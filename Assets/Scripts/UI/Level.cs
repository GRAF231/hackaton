﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Zenject;
using Data;
using Game;

namespace UI
{
    public class Level : MonoBehaviour
    {
        [Inject]
        protected IGame game;

        [Inject]
        protected MenuManager menu;

        [SerializeField] 
        protected Image[] stars;
        protected int levelID;

        [SerializeField]
        protected Sprite starYellow, starWhite, buttonEnable, buttonDisable;

        protected Button btn;
        protected Image btnImage;

        [SerializeField] 
        protected TextMeshProUGUI levelText;

        public void Init(LevelProgressInfo info, int id)
        {
            btn = GetComponent<Button>();
            btnImage = GetComponent<Image>();

            levelID = id;
            levelText.text = $"Level {levelID}";

            for (int i = 0; i < stars.Length; i++)
                stars[i].sprite = starWhite;

            for (int i = 0; i < info.stars; i++)
                stars[i].sprite = starYellow; 

            if(info.isOpened)
            {
                btn.interactable = true;
                btnImage.sprite = buttonEnable;
                for (int i = 0; i < stars.Length; i++)
                    stars[i].gameObject.SetActive(true);
            }
            else
            {
                btn.interactable = false;
                btnImage.sprite = buttonDisable;
                for (int i = 0; i < stars.Length; i++)
                    stars[i].gameObject.SetActive(false);
            }
        }
        
        public void ClickHandler()
        {
            game.StartLevel(levelID);
            menu.SwitchPanel(ActivityType.Game);
        }

        public class Factory : PlaceholderFactory<Level> { }
    }
}