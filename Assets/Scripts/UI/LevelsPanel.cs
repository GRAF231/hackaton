﻿using Data;
using Game;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI
{
    public class LevelsPanel : UIPanel
    {
        [Inject]
        protected IGame game;

        [Inject]
        protected IProgress progress;

        [Inject]
        protected ILoader loader;

        [Inject]
        protected IAudioManager aManager;

        [Inject]
        protected Level.Factory levelButtonFactory;

        protected List<Level> buttons = new List<Level>();

        [SerializeField]
        protected Transform container;

        [SerializeField]
        protected TextMeshProUGUI totalStar;

<<<<<<< HEAD
=======

        void Awake()
        {
            menuManager.AddPanel(ActivityType.Levels, this);

            for (int i = 0; i < loader.Levels; i++)
            {
                CreateButton(i + 1);
            }
            HideUI();
        }

>>>>>>> 4923cf0e1257ba4f27d0c768a9009ee72ab10395
        public override void ShowUI()
        {
            base.ShowUI();

            totalStar.text = progress.StarsSum.ToString();

            for (int i = 0; i < buttons.Count; i++)
            {
                var info = progress.GetInfo(i + 1);
                buttons[i].Init(info, i + 1);
            }
        }

        public override void HideUI()
        {
            base.HideUI();
        }

        void CreateButton(int levelID)
        {
            var levelButton = levelButtonFactory.Create();
            levelButton.transform.SetParent(container);
            levelButton.transform.localScale = Vector3.one;

            var info = progress.GetInfo(levelID);
            levelButton.Init(info, levelID);

            buttons.Add(levelButton);
        }

        public void Back()
        {
            aManager.PlayOneShot("UI/close");
            menuManager.SwitchPanel(ActivityType.Main);
        }
    }
}