﻿using Game;
using UnityEngine;
using Zenject;

namespace UI
{
    public class PausePanel : UIPanel
    {
        [Inject]
        protected IGame game;
        
        [Inject]
        protected IAudioManager aManager;

        public override void ShowUI()
        {
            base.ShowUI();
            Time.timeScale = 0;
        }
        public override void HideUI()
        {
            base.HideUI();
            Time.timeScale = 1f;
        }
        public void ToMainMenu()
        {
            aManager.PlayOneShot("UI/Klick");
            game.Destroy();
            menuManager.SwitchPanel(ActivityType.Main);
        }
        public void Restart()
        {
            aManager.PlayOneShot("UI/Klick");
            game.Restart();
            menuManager.SwitchPanel(ActivityType.Game);
        }
        public void Back()
        {
            aManager.PlayOneShot("UI/close");
            menuManager.SwitchPanel(ActivityType.Game);
        }
    }
}