﻿using Zenject;

namespace UI
{   
    public class MainPanel : UIPanel
    {
        [Inject]
        protected IAudioManager aManager;

        public void Levels()
        {
            aManager.PlayOneShot("UI/Klick");
            menuManager.SwitchPanel(ActivityType.Levels);
        }
        public void ToOptions()
        {
            aManager.PlayOneShot("UI/Klick");
            menuManager.SwitchPanel(ActivityType.Options);
        }
        public void ToAbout()
        {
            aManager.PlayOneShot("UI/Klick");
            menuManager.SwitchPanel(ActivityType.About);
        }

        public override void ShowUI()
        {
            base.ShowUI();

            aManager.StopBackgroundMusic();
            aManager.PlayBackgroundMusic("SoundGame/mainMenu");
        }
    }
}