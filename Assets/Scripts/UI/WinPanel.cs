﻿using Zenject;

using Game;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class WinPanel : UIPanel
    {
        [Inject]
        protected IGame gManager;

        [Inject]
        protected IAudioManager aManager;

        [SerializeField]
        protected Image[] stars;
        [SerializeField]
        protected Sprite starYellow, starWhite;

        void Awake()
        {
            menuManager.AddPanel(type, this);
            HideUI();

            gManager.State.OnPropertyChange += state =>
            {
                if(state == GameState.Win)
                {
                    ShowUI();
                }
            };
        }

        public override void ShowUI()
        {
            base.ShowUI();

            aManager.PlayOneShot("UI/Win");

            for (int i = 0; i < stars.Length; i++)
                stars[i].sprite = starWhite;

            for (int i = 0; i < gManager.Stars.Value; i++)
                stars[i].sprite = starYellow;
        }

        public void ToMainMenu()
        {
            gManager.Destroy();
            menuManager.SwitchPanel(ActivityType.Main);
        }
    }
}