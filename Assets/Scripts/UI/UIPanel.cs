﻿using UnityEngine;
using Zenject;

namespace UI
{
    public class UIPanel : MonoBehaviour, IPanel
    {
        [Inject]
        protected MenuManager menuManager;

        [SerializeField]
        protected ActivityType type;

        private void Awake()
        {
            menuManager.AddPanel(type, this);
            HideUI();
        }

        public virtual void ShowUI()
        {
            gameObject.SetActive(true);
        }

        public virtual void HideUI()
        {
            gameObject.SetActive(false);
        }
    }
}