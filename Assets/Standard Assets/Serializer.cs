using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;

using UnityEngine;

namespace AkilGames.Serializing
{
    public static class Serializer
    {
        public static void XMLSerialize<T>(string path, T obj) where T : new()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            var formatter = new XmlSerializer(typeof(T));
            using (var fs = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(fs, obj);
            }
        }

        public static T XMLDeserialize<T>(string path) where T : new()
        {
            T res;
            try
            {
                var formatter = new XmlSerializer(typeof(T));
                using (var fs = new FileStream(path, FileMode.Open))
                {
                    res = (T)formatter.Deserialize(fs);
                }
            }
            catch(System.Exception e)
            {
                Debug.Log(e.Message);
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                res = new T();
                XMLSerialize(path, res);
            }
            return res;
        }

        public static void Serialize<T>(string path, T obj) where T : new()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, obj);
            }
        }

        public static T Deserialize<T>(string path) where T : new()
        {
            T res;
            try
            {
                var formatter = new BinaryFormatter();
                using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    res = (T)formatter.Deserialize(fs);
                }
            }
            catch
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                res = new T();
                Serialize(path, res);
            }
            return res;
        }

        public static void XMLSerializeToResources<T>(string path, T obj) where T : new()
        {
            path = Application.dataPath + @"/Resources/" + path + ".xml";
            XMLSerialize<T>(path, obj);
        }

        public static T XMLDeserializeFromResources<T>(string path) where T : new()
        {
            T res;

            try
            {
                var xmlText = Resources.Load(path);
                var xDoc = new XmlDocument();
                xDoc.LoadXml((xmlText as TextAsset).text);
                var formatter = new XmlSerializer(typeof(T));

                using (var xmlReader = new XmlNodeReader(xDoc))
                {
                    res = (T)formatter.Deserialize(xmlReader);
                }
            }
            catch(System.Exception e)
            {
                Debug.Log(e.Message);
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                res = new T();
            }

            return res;
        }
    }
}