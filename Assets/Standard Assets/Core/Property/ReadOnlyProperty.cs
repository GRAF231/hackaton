﻿using System;

namespace Graf.Properties
{
    public class ReadOnlyProperties<T>
    {
        protected T property;

        public T Property
        {
            get
            {
                return property;
            }
        }
    }
}