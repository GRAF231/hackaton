﻿using UnityEngine;
namespace Graf.Properties
{
    public class BoolHashed
    {
        string key;

        public BoolHashed(string key)
        {
            this.key = key;
        }

        public bool Property
        {
            get
            {
                bool temp;
                bool.TryParse(PlayerPrefs.GetString($"FLOAT_HASHED_{key}"), out temp);
                return temp;
            }
            set
            {
                PlayerPrefs.SetString($"FLOAT_HASHED_{key}", value.ToString());
            }
        }
    }
}